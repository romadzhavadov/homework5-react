import { SET_GOODS } from "../actions/goodsActions"

export const setGoods = (value) => ({type: SET_GOODS, payload: value})

export const fetchGoods = () => {
    return async (dispatch) => {
        const data = await fetch('./goods.json').then(res => res.json())
        const savedFavGoods = localStorage.getItem('favGoods');
        if (savedFavGoods) {
            dispatch(setGoods(JSON.parse(savedFavGoods)))
        } else {
            dispatch(setGoods(data))
        }

    }
}


