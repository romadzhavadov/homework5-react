import { SET_MODAL, SET_HEADER, SET_TEXT, ADD_ACTIONS } from "../actions/modalAction";

export const setIsOpenModal = (value) => ({type:SET_MODAL, payload: value})

export const setHeaderModal = (value) => ({type:SET_HEADER, payload: value})

export const setTextModal = (value) => ({type:SET_TEXT , payload: value})

export const addActionsModal = (value) => ({type:ADD_ACTIONS , payload: value})