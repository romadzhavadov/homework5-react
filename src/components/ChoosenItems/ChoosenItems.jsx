import React from "react";
import ChoosenCard from "../ChoosenCard/ChoosenCard";
import { shallowEqual, useSelector } from "react-redux";
import GoodsForm from "../forms/goodsForm";
import styles from './ChoosenItems.module.scss'

const ChoosenItems = () => {

    const items = useSelector(state => state.chosen.choosenArr, shallowEqual)
    
    return(
        <section className={styles.cardWrap}> 
            <div className={styles.cardBox}>
                {items.length > 0 ? <h2>Вибрані товари:</h2> : null}
                {items.length > 0 ? items.map(({name, price, url, id, isFavourite, color, count}) => <ChoosenCard key={id} name={name} price={price} url={url} id={id} color={color} isFavourite={isFavourite} count={count} />) : <p className="noitems"> No items have been added </p> }
            </div>
            <div className={styles.orderBox}>
                {items.length > 0 ? <GoodsForm /> : null}
            </div>
        </section>
    )

}


export default ChoosenItems;