import React, { memo } from "react";
import styles from './Card.module.scss'
import Button from "../Button/Button";
// import starIcon from "../../asset/star.svg"
// import StarIcon from "../svgComponents/StarIcon";
import { ReactComponent as StarIcon} from "../../asset/star.svg"
import classNames from "classnames";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { addToChosenGoods } from "../../redux/actionCreators/chosenGoodsAction";
import { setIsOpenModal, setHeaderModal, setTextModal, addActionsModal } from "../../redux/actionCreators/modalActionCreators";
import { setFavouriteCard } from "../../redux/actionCreators/favouriteActionCreators";


const Card = (props) => {

    const {name, price, url, id, color, isFavourite} = props;

    const dispatch = useDispatch()
    const {isOpen} = useSelector(state => state.modal)

    const clickAddGoods = (e) => {
        dispatch(setHeaderModal('Do you want to add?'))
        dispatch(setTextModal(`By clicking OK, you will add the corresponding product to the cart.`))
        dispatch(addActionsModal(
        <>
            <button className='btnActions' onClick={handleClicktoModal}>Ok</button>
            <button className='btnActions' onClick={()=>{dispatch(setIsOpenModal(false))}}>Cancel</button>
        </>))

        if(!isOpen) {
          dispatch(setIsOpenModal(true))
        } else {
          dispatch(setIsOpenModal(false))
        }
    }
    
    const changeColorStar = (e) => {
        e.preventDefault()
        dispatch(setFavouriteCard(id))     
    }

    const handleClicktoModal = (e) => {
        const newState = {name, price, url, id, color, isFavourite, count:1}
        dispatch(addToChosenGoods(newState));
        dispatch(setIsOpenModal(false))
    }

    return(
        <div className={styles.wrapper}>
            <a href="#" className={styles.card}>
                <div className={styles.favouritesWrap}>
                    {/* <a href="#" className={classNames(styles.favourites, {[styles.active] : isFavourite})} onClick={changeColorStar}>
                        <StarIcon/>
                    </a> */}
                    <div className={classNames(styles.favourites, {[styles.active] : isFavourite})} onClick={changeColorStar}>
                    <StarIcon/>
                    </div>
                </div>

                
                <img className={styles.img} src={url} alt={name} />
                <p className={styles.caption}>{name}</p>
                <div className={styles.infoWrap}>
                    <span className={styles.info}>your price: {price} $</span>
                    <span className={styles.desc}> color: {color}</span>
                </div>
                <Button onClick={clickAddGoods} >Add to cart</Button>
            </a>
        </div>
    )
}

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    id: PropTypes.number,
    color: PropTypes.string,
    isFavourite: PropTypes.bool,
    count: PropTypes.number
}

Card.defaultProps = {
    name: 'text',
    price: 'text',
    url: 'text',
    id: 0,
    color: 'text',
    isFavourite: false,
    count: 0
}

export default memo(Card);



