import React, {memo} from "react";
import Card from "../Card/Card";
import {shallowEqual, useSelector } from "react-redux";
import styles from "./itemsContainer.module.scss"

const ItemsContainer = () => {

    const items = useSelector(state => state.goods.goodsArr, shallowEqual)

    return(
        <section className={styles.cardWrap}>
            <h2> Всі товари:</h2>
            {items && items.map(({name, price, url, id, color, isFavourite }) => <Card key={id} name={name} price={price} url={url} id={id} color={color} isFavourite={isFavourite} /> )}

        </section>
    )

}

export default memo(ItemsContainer);