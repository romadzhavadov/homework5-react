import {PureComponent} from "react";
import oopsImage from '../../asset/oops.jpg';
import styles from './SomethingWentWrong.module.scss';

class SomethingWentWrong extends PureComponent {
    render(){
        return (
            <section className={styles.root}>
                <h1>Something went wrong...</h1>
                <img src={oopsImage} alt="Oooops" width={300} height={225} />
                <p>Try to reload page</p>
                <button onClick={() => window?.location?.reload()}>Reload</button>
            </section>
        );
    }
}

export default SomethingWentWrong;
