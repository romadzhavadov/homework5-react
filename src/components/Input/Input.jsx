import React from "react";
import { useField } from "formik";
import PropTypes from "prop-types";
import styles from "../Input/Input.module.scss"

const Input = ({type, labelText, ...props}) => {

    const [field, meta ] = useField(props.name)

    const { error, touched } = meta;

    return(

        <div className={styles.inputContainer}>
            {/* {props.name || field.name} */}
            <label className={styles.labelText} htmlFor={props.id}>{labelText}</label>
            <input className={styles.inputText} type={type} {...field} {...props} />

            {touched && error && <p className={styles.err}>*{error}</p>}
        </div>
    )
}

Input.propTypes = {
    type: PropTypes.string,
    labelText: PropTypes.string,
}
Input.defaultProps ={
    type: 'text',
    labelText: 'text',
}

export default Input;