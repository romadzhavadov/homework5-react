import React, {memo} from "react";
import Card from "../Card/Card";
import {shallowEqual, useSelector } from "react-redux";
import styles from "./FavouriteContainer.module.scss"

const ItemsContainer = () => {

    const items = useSelector(state => state.goods.goodsArr, shallowEqual)

    const hasFavourites = items.some(item => item.isFavourite);

    return(
        <section className={styles.cardWrap}>
            {hasFavourites ? <h2> Збережені товари:</h2> : null}
            {hasFavourites ? items.filter(item => item.isFavourite === true).map(({name, price, url, id, color, isFavourite }) => <Card key={id} name={name} price={price} url={url} id={id} color={color} isFavourite={isFavourite} /> ) : <p className="noitems"> No items have been added </p>}
        </section>
    )

}

export default memo(ItemsContainer);