import React from "react";
import { Routes, Route } from 'react-router-dom';
import HomePage from "./pages/Home/HomePage";
import Favourite from "./pages/Favourite/Favourire";
import Choosen from "./pages/Choosen/Choosen";

const AppRoutes = () => {

    return(
        <Routes>
            <Route path='/' element={ <HomePage />} />
            <Route path='/favourite' element={ <Favourite />} />
            <Route path='/choosen' element={ <Choosen />} />
        </Routes>
    )
}

export default AppRoutes;