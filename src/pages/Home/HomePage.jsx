import React from "react";
import ItemsContainer from "../../components/ItemsContainer/itemsContainer";
// import ChoosenItems from "../../components/ChoosenItems/ChoosenItems";

const HomePage = () => {

    return(
        <div className="container">
            <ItemsContainer />
            {/* <ChoosenItems /> */}
        </div>
    )
}

export default HomePage;